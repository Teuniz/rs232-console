#
#
# Author: Teunis van Beelen
#
# email: teuniz@protonmail.com
#
#

CC = gcc
CFLAGS = -O2 -Wall -Wextra -Wshadow -Wformat-nonliteral -Wformat-security -Wtype-limits

objects = main.o rs232.o

all: rs232-console

rs232-console : $(objects)
	$(CC) $(objects) -o rs232-console

main.o : main.c rs232.h
	$(CC) $(CFLAGS) -c main.c -o main.o

rs232.o : rs232.h rs232.c
	$(CC) $(CFLAGS) -c rs232.c -o rs232.o

clean :
	$(RM) rs232-console $(objects)

#
#
#
#





