/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2019 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <string.h>

#include "rs232.h"


volatile sig_atomic_t sig_flag=0;

void signal_catch_func(__attribute__((unused)) int sig)
{
  sig_flag = 1;
}


int main(int argc, char *argv[])
{
  int cport=16, flowctl=1, baudr=115200, opt, n;

  char cportstr[16]={"ttyUSB0"},
       modestr[16]={"8N1"},
       flowctlstr[16]={"hardware"},
       txbuf[4096],
       rxbuf[4096];

  setlocale(LC_ALL, "C");

  signal(SIGINT, signal_catch_func);

  while((opt = getopt(argc, argv, "p:b:m:f:")) != -1)
  {
    switch (opt)
    {
      case 'p': strncpy(cportstr, optarg, 15);
                cportstr[15] = 0;
                break;
      case 'b': baudr = atoi(optarg);
                break;
      case 'm': strncpy(modestr, optarg, 15);
                modestr[15] = 0;
                break;
      case 'f': strncpy(flowctlstr, optarg, 15);
                flowctlstr[15] = 0;
                break;
      default:  fprintf(stderr, "Usage: %s <-p port> <-b baudrate> <-m mode> <-f flowcontrol>\n"
                                "e.g.: %s -p ttyUSB0 -b 115200 -m 8N1 -f hardware\n"
                                , argv[0], argv[0]);
                exit(EXIT_FAILURE);
    }
  }

  cport = RS232_GetPortnr(cportstr);
/*
  printf("cportstr=%s; baudrate=%i; mode=%s; flowcontrol=%s;\n", cportstr, baudr, modestr, flowctlstr);
*/
  if(cport < 0)
  {
    fprintf(stderr, "Unrecognized value for port\n");
    exit(EXIT_FAILURE);
  }

  if((baudr < 50) || (baudr > 4000000))
  {
    fprintf(stderr, "Unrecognized value for baudrate\n");
    exit(EXIT_FAILURE);
  }

  if(strlen(modestr) != 3)
  {
    fprintf(stderr, "Unrecognized value for mode\n");
    exit(EXIT_FAILURE);
  }

  switch(modestr[0])
  {
    case '8':
    case '7':
    case '6':
    case '5': break;
    default : fprintf(stderr, "Unrecognized value for mode\n");
              exit(EXIT_FAILURE);
  }

  switch(modestr[1])
  {
    case 'N':
    case 'n':
    case 'E':
    case 'e':
    case 'O':
    case 'o': break;
    default : fprintf(stderr, "Unrecognized value for mode\n");
              exit(EXIT_FAILURE);
  }

  switch(modestr[2])
  {
    case '1':
    case '2': break;
    default : fprintf(stderr, "Unrecognized value for mode\n");
              exit(EXIT_FAILURE);
  }

  if(!strcmp(flowctlstr, "none"))
  {
    flowctl = 0;
  }
  else if(!strcmp(flowctlstr, "hardware"))
    {
      flowctl = 1;
    }
    else
    {
      fprintf(stderr, "Unrecognized value for flowcontrol\n");
      exit(EXIT_FAILURE);
    }

  if(RS232_OpenComport(cport, baudr, modestr, flowctl))
  {
    fprintf(stderr, "Can not open serial port /dev/%s\n", cportstr);
    exit(EXIT_FAILURE);
  }

  if(fcntl(STDIN_FILENO, F_SETFL, fcntl(STDIN_FILENO, F_GETFL) | O_NONBLOCK) == -1)
  {
    perror("fcntl() returned an error");
    exit(EXIT_FAILURE);
  }

  while(1)
  {
    n = read(STDIN_FILENO, txbuf, 4096);

    if(n > 0)
    {
      if(RS232_SendBuf(cport, (unsigned char *)txbuf, n) != n)
      {
        fprintf(stderr, "An error occurred while writing to the serial port\n");
        RS232_CloseComport(cport);
        exit(EXIT_FAILURE);
      }
    }

    n = RS232_PollComport(cport, (unsigned char *)rxbuf, 4096);
    if(n < 0)
    {
      fprintf(stderr, "An error occurred while reading from the serial port\n");
      RS232_CloseComport(cport);
      exit(EXIT_FAILURE);
    }
    else if(n > 0)
    {
      write(STDOUT_FILENO, rxbuf, n);
    }

    usleep(20000);

    if(sig_flag)  break;  /* ctl-c pressed */
  }

  RS232_CloseComport(cport);

  printf("\n");

  exit(EXIT_SUCCESS);
}








